import React from 'react'

function Header() {
	return (
		<header className='header'>
			<img
				alt='logo'
				src='https://static.rfstat.com/renderforest/images/v2/logo-homepage/flat_3.png'
			/>
		</header>
	)
}
export default Header
